# The Monte Carlo Simulation

This program is for executing Monte Carlo Simulations. These should be applied for error calculation in scientific data processing.
The main purpose is to calculate error envelopes for plots.

## Mathematical concept

1. Prerequisites:

  * Multiple datapoints on a xy-(Scatter-)plots
  * x and y Errors for the datapoints
  * Assuming a normal distribution for proablity of the error
  * Optional: linear correlation and linear fit for the datapooints (my case)

2. Execution:

  * Generate x (ca. 50  - 500) simulated datapoints based on the normal distributed errors
  * Create linear fits for each of the "runs" of generation
  * For x(n) in fixed distances on the x-axis calculate mean and stdev of the y values for the (ca. 50 -500) linear fits
  * Calculate a linear fit for the mean values (x(n)) of all simulated linear fits and plot it
  * Calculate the stdev for all x(n) and plot an "error envelope" around the linear fit

  * This should produce something similar to [the second ploat on this page](http://www.randalolson.com/2014/06/28/how-to-make-beautiful-data-visualizations-in-python-with-matplotlib/)

## Program concept

This should be a lightweight python script applicable many times. It should require a dataset as input as well as the number of "simulation cycles" x.
Then it should do the computing and maybe even plot everything. I guess It's best to split the calculating and plotting bit into two parts. As I would like a script
that I can reuse for (nearly all my data) it shouldn't be tailored to a specific case. It should rather just takek my data (maybe from Excel) do the Monte Carlo Simulation,
and output the data again.

Developing this script should take some time, as I'm barely beginning python and the statistics behind it isn't really entry level. So any help at all is really appreciated :)
