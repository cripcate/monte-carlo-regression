# Main Script for the Monte Carlo Simulation of correlating x-y errors

import numpy as np
import pandas as pd

# TODO: Load the data: Either from a .csv or from Libre Office Calc

# TODO: Set up everything the simulation needs:
#       - Normal distribution for x and y Errors
#       - Number of simulation cycles x
#       -

# TODO: Monte Carlo Simulation:
#       1. Generate one data point for every existing datapoint based on the normal-distributed errors
#       2. Calculate a linear fit through the generated datapooints
#       3. Do this cycle x times

# TODO: Evaluation of the Simulation
#       1. For x(n) in equal distances on the x-axis (e.g. 0, 1, 2, 3, ...) calculate mean and stdev of all linear fits
#       2. Output the mean linear fit and the calculated standard deviations (to later plot an error envelope)
